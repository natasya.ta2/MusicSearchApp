package com.natasya.musicsearch;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.natasya.musicsearch.adapter.ListAlbumsAdapter;
import com.natasya.musicsearch.api.Api;
import com.natasya.musicsearch.spotifymodel.SpotifyItems;
import com.natasya.musicsearch.spotifymodel.SpotifyNewRelease;
import com.natasya.musicsearch.spotifymodel.SpotifyToken;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewRelease extends AppCompatActivity {

    ImageView musicImg;
    TextView message, spotifyLinkText;
    LinearLayout refreshBtn, refreshBtnTop, spotifyLink, spotifyLinkLayout;
    BottomNavigationView bottomNav;
    ListView listAlbums;
    ListAlbumsAdapter listAlbumsAdapter;
    LoadingDialog loadingDialog;
    DBHandler db;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_release);

        refreshBtnTop = findViewById(R.id.refreshBtnTop);
        musicImg = findViewById(R.id.musicImg);
        refreshBtn = findViewById(R.id.refreshBtn);
        message = findViewById(R.id.message);
        bottomNav = findViewById(R.id.bottomNav);
        listAlbums = findViewById(R.id.listAlbum);
        loadingDialog = new LoadingDialog(NewRelease.this);
        spotifyLink = findViewById(R.id.spotifyLink);
        spotifyLinkText = findViewById(R.id.spotifyLinkText);
        spotifyLinkLayout = findViewById(R.id.spotifyLinkLayout);
        db = new DBHandler(NewRelease.this);

        Cursor authData = db.getTokenData(db.TABLE_NAME);
        if(authData.getCount()!=0){
            if(authData.moveToFirst()){

                token = authData.getString(0);
            }
        }

        refreshBtnTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNewRelease(token, "init");
            }
        });

        getNewRelease(token, "init");

        bottomNav.setSelectedItemId(R.id.newAlbumNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.newAlbumNav:

                        break;

                    case R.id.trackNav:
                        finish();
                        Intent intent = new Intent(NewRelease.this, BrowseTrack.class);
                        startActivity(intent);
                        break;

                    case R.id.artistNav:
                        finish();
                        Intent intent2 = new Intent(NewRelease.this, BrowseArtist.class);
                        startActivity(intent2);
                        break;

                }

                return false;
            }
        });



        setSpotifyLinkLayout();


    }

    public void setSpotifyLinkLayout(){
        if(listAlbums.isShown()){
            spotifyLinkLayout.setVisibility(View.VISIBLE);
        }else{
            spotifyLinkLayout.setVisibility(View.GONE);
        }
    }

    public void getNewRelease(String token, String menu){

        if(menu.equals("init")){
            loadingDialog.onLoading();
        }

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_API_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call <SpotifyNewRelease> call = api.getNewReleases("ID", "10",  "Bearer "+token);

        call.enqueue(new Callback<SpotifyNewRelease>() {
            @Override
            public void onResponse(Call<SpotifyNewRelease> call, Response<SpotifyNewRelease> response) {

                SpotifyNewRelease album = response.body();
                if(response.code()!= 200){

                        loadingDialog.stopLoading();
                        musicImg.setVisibility(View.VISIBLE);
                        message.setVisibility(View.VISIBLE);
                        message.setText("Sorry, please refresh to try again!");
                        listAlbums.setVisibility(View.GONE);
                        refreshBtn.setVisibility(View.VISIBLE);
                        refreshBtn.setEnabled(true);
                        refreshBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingDialog.onLoading();
                                getToken();
                            }
                        });
                        setSpotifyLinkLayout();


                }else{
                    loadingDialog.stopLoading();
                    if(album.getAlbums().getItems().size() != 0){
                        final List<SpotifyItems>results = album.getAlbums().getItems();
                        listAlbumsAdapter = new ListAlbumsAdapter(NewRelease.this, R.layout.row_album, results);
                        listAlbums.setAdapter(listAlbumsAdapter);
                        listAlbums.setVisibility(View.VISIBLE);
                        musicImg.setVisibility(View.GONE);
                        message.setVisibility(View.GONE);
                        refreshBtn.setVisibility(View.GONE);


                        spotifyLink.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PackageManager pm = getPackageManager();
                                boolean isSpotifyInstalled;
                                try {
                                    pm.getPackageInfo("com.spotify.music", 0);
                                    isSpotifyInstalled = true;
                                } catch (PackageManager.NameNotFoundException e) {
                                    isSpotifyInstalled = false;
                                }

                                if (isSpotifyInstalled == true){
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse("spotify:album:" + results.get(0).getId()));
                                    intent.putExtra(Intent.EXTRA_REFERRER,
                                            Uri.parse("android-app://" + NewRelease.this.getPackageName()));
                                    startActivity(intent);
                                    //Toast.makeText(NewRelease.this, "Installed!", Toast.LENGTH_SHORT).show();
                                }else{

                                    final String appPackageName = "com.spotify.music";
                                    final String referrer = "adjust_campaign=PACKAGE_NAME&adjust_tracker=ndjczk&utm_source=adjust_preinstall";

                                    try {
                                        Uri uri = Uri.parse("market://details")
                                                .buildUpon()
                                                .appendQueryParameter("id", appPackageName)
                                                .appendQueryParameter("referrer", referrer)
                                                .build();
                                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    } catch (android.content.ActivityNotFoundException ignored) {
                                        Uri uri = Uri.parse("https://play.google.com/store/apps/details")
                                                .buildUpon()
                                                .appendQueryParameter("id", appPackageName)
                                                .appendQueryParameter("referrer", referrer)
                                                .build();
                                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    }
                                    //Toast.makeText(NewRelease.this, "Not Installed!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });


                        PackageManager pm = getPackageManager();
                        boolean isSpotifyInstalled;
                        try {
                            pm.getPackageInfo("com.spotify.music", 0);
                            isSpotifyInstalled = true;
                        } catch (PackageManager.NameNotFoundException e) {
                            isSpotifyInstalled = false;
                        }

                        if(isSpotifyInstalled == true){
                            spotifyLinkText.setText("OPEN SPOTIFY");
                        }else{
                            spotifyLinkText.setText("GET SPOTIFY FREE");
                        }

                        setSpotifyLinkLayout();
                    }else{
                        listAlbums.setVisibility(View.GONE);
                        musicImg.setVisibility(View.VISIBLE);
                        refreshBtn.setVisibility(View.GONE);
                        message.setVisibility(View.VISIBLE);
                        message.setText("No data found.");

                        setSpotifyLinkLayout();
                    }

                }

            }

            @Override
            public void onFailure(Call<SpotifyNewRelease> call, Throwable t) {
                loadingDialog.stopLoading();
                musicImg.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                message.setText("Sorry, please refresh to try again!");
                listAlbums.setVisibility(View.GONE);
                refreshBtn.setVisibility(View.VISIBLE);
                refreshBtn.setEnabled(true);
                refreshBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingDialog.onLoading();
                        getToken();
                    }
                });
                setSpotifyLinkLayout();

            }
        });


    }

    public void getToken(){

        //loadingDialog.onLoading();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_TOKEN_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyToken> call = api.getSpotifyToken("client_credentials", "application/x-www-form-urlencoded", "Basic NTdmMmI1ZThhMWEyNGNiYjkxYjY2NTFkM2Q0ZDhkYmY6M2IyNTE0ZWMzNDMwNDBiMDgxNmQ4NzA3M2I1ZTQ0YjU=");
        call.enqueue(new Callback<SpotifyToken>() {
            @Override
            public void onResponse(Call<SpotifyToken> call, Response<SpotifyToken> response) {

                SpotifyToken token = response.body();
                if (response.code() != 200) {
                    loadingDialog.stopLoading();
                    musicImg.setVisibility(View.VISIBLE);
                    message.setVisibility(View.VISIBLE);
                    message.setText("Sorry, please refresh to try again!");
                    listAlbums.setVisibility(View.GONE);
                    refreshBtn.setVisibility(View.VISIBLE);
                    refreshBtn.setEnabled(true);
                    refreshBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadingDialog.onLoading();
                            getToken();
                        }
                    });
                    setSpotifyLinkLayout();
                } else {
                    Cursor cursor = db.getTokenData(db.TABLE_NAME);
                    if(cursor.getCount()==0){
                        db.insertAuthData(token.getAccess_token());
                    }else{
                        db.deleteData(db.TABLE_NAME);
                        db.insertAuthData(token.getAccess_token());
                    }

                    musicImg.setVisibility(View.VISIBLE);
                    message.setVisibility(View.VISIBLE);
                    message.setText("Sorry, please refresh to try again!");
                    refreshBtn.setVisibility(View.VISIBLE);
                    listAlbums.setVisibility(View.GONE);

                    getNewRelease(token.getAccess_token(), "");
                    setSpotifyLinkLayout();
                }

            }

            @Override
            public void onFailure(Call<SpotifyToken> call, Throwable t) {
                loadingDialog.stopLoading();
                musicImg.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                message.setText("Sorry, please refresh to try again!");
                listAlbums.setVisibility(View.GONE);
                refreshBtn.setVisibility(View.VISIBLE);
                refreshBtn.setEnabled(true);
                refreshBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingDialog.onLoading();
                        getToken();
                    }
                });
                setSpotifyLinkLayout();

            }
        });
    }


    @Override
    public void onBackPressed() {
       finish();
    }

}
