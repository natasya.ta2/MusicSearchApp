package com.natasya.musicsearch;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.natasya.musicsearch.adapter.ListTracksInAlbumAdapter;
import com.natasya.musicsearch.spotifymodel.SpotifyAlbumTracks;

import java.util.ArrayList;

public class ViewTracks extends AppCompatActivity {

    String album, artist, albumID;
    ArrayList<SpotifyAlbumTracks> trackItems;
    ListTracksInAlbumAdapter tracklistAdapter;
    ListView listTracks;
    TextView albumName, artistName, spotifyLinkText;
    LinearLayout closeBtn, spotifyLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_tracks);

        Intent intent = getIntent();
        album= (String) intent.getSerializableExtra("albumName");
        artist= (String) intent.getSerializableExtra("artistName");
        albumID= (String) intent.getSerializableExtra("albumID");

        spotifyLink = findViewById(R.id.spotifyLink);
        spotifyLinkText = findViewById(R.id.spotifyLinkText);

        trackItems = (ArrayList<SpotifyAlbumTracks>) intent.getSerializableExtra("tracklist");
        albumName = findViewById(R.id.albumName);
        artistName = findViewById(R.id.artistName);
        listTracks = findViewById(R.id.listTracks);
        closeBtn = findViewById(R.id.closeBtn);

        tracklistAdapter = new ListTracksInAlbumAdapter(ViewTracks.this, R.layout.row_track_list, trackItems);
        listTracks.setAdapter(tracklistAdapter);
        listTracks.setVisibility(View.VISIBLE);
        artistName.setText(artist);
        albumName.setText(album);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        spotifyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                boolean isSpotifyInstalled;
                try {
                    pm.getPackageInfo("com.spotify.music", 0);
                    isSpotifyInstalled = true;
                } catch (PackageManager.NameNotFoundException e) {
                    isSpotifyInstalled = false;
                }

                if (isSpotifyInstalled == true){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("spotify:album:"+ albumID));
                    intent.putExtra(Intent.EXTRA_REFERRER,
                            Uri.parse("android-app://" + getApplicationContext().getPackageName()));
                    startActivity(intent);
                    //Toast.makeText(ViewTracks.this, "Installed!", Toast.LENGTH_SHORT).show();
                }else{

                    final String appPackageName = "com.spotify.music";
                    final String referrer = "adjust_campaign=PACKAGE_NAME&adjust_tracker=ndjczk&utm_source=adjust_preinstall";

                    try {
                        Uri uri = Uri.parse("market://details")
                                .buildUpon()
                                .appendQueryParameter("id", appPackageName)
                                .appendQueryParameter("referrer", referrer)
                                .build();
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    } catch (android.content.ActivityNotFoundException ignored) {
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details")
                                .buildUpon()
                                .appendQueryParameter("id", appPackageName)
                                .appendQueryParameter("referrer", referrer)
                                .build();
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    }
                    //Toast.makeText(ViewTracks.this, "Not Installed!", Toast.LENGTH_SHORT).show();
                }

            }
        });


        PackageManager pm = getPackageManager();
        boolean isSpotifyInstalled;
        try {
            pm.getPackageInfo("com.spotify.music", 0);
            isSpotifyInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            isSpotifyInstalled = false;
        }

        if(isSpotifyInstalled == true){
            spotifyLinkText.setText("OPEN SPOTIFY");
        }else{
            spotifyLinkText.setText("GET SPOTIFY FREE");
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
