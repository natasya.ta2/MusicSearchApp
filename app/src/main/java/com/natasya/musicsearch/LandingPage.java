package com.natasya.musicsearch;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.natasya.musicsearch.api.Api;
import com.natasya.musicsearch.spotifymodel.SpotifyToken;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LandingPage extends AppCompatActivity {


    LinearLayout startBtn;
    LoadingDialog loadingDialog;
    DBHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_page);

        startBtn = findViewById(R.id.startBtn);
        loadingDialog = new LoadingDialog(LandingPage.this);
        db = new DBHandler(LandingPage.this);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getToken();

            }
        });
    }

    public void getToken(){
        loadingDialog.onLoading();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_TOKEN_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyToken> call = api.getSpotifyToken("client_credentials", "application/x-www-form-urlencoded",
                "Basic NTdmMmI1ZThhMWEyNGNiYjkxYjY2NTFkM2Q0ZDhkYmY6M2IyNTE0ZWMzNDMwNDBiMDgxNmQ4NzA3M2I1ZTQ0YjU=");
        call.enqueue(new Callback<SpotifyToken>() {
            @Override
            public void onResponse(Call<SpotifyToken> call, Response<SpotifyToken> response) {


                loadingDialog.stopLoading();
                SpotifyToken token = response.body();
                if (response.code() != 200) {
                    Toast.makeText(LandingPage.this, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();

                } else {
                    //insert to db
                    Cursor cursor = db.getTokenData(db.TABLE_NAME);
                    if(cursor.getCount()==0){
                        db.insertAuthData(token.getAccess_token());
                    }else{
                        db.deleteData(db.TABLE_NAME);
                        db.insertAuthData(token.getAccess_token());
                    }
                    finish();
                    Intent intent = new Intent(LandingPage.this, NewRelease.class);
                    startActivity(intent);

                }

            }

            @Override
            public void onFailure(Call<SpotifyToken> call, Throwable t) {
                loadingDialog.stopLoading();
                Toast.makeText(LandingPage.this, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
            }
        });
    }



}

