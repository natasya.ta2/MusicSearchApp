package com.natasya.musicsearch.api;

import com.natasya.musicsearch.spotifymodel.SpotifyAudioFeatures;
import com.natasya.musicsearch.spotifymodel.SpotifyNewRelease;
import com.natasya.musicsearch.spotifymodel.SpotifySearchByArtist;
import com.natasya.musicsearch.spotifymodel.SpotifySearchByTrack;
import com.natasya.musicsearch.spotifymodel.SpotifyToken;
import com.natasya.musicsearch.spotifymodel.SpotifyTracksInAlbum;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {
    String SPOTIFY_API_URL = "https://api.spotify.com/v1/";
    String SPOTIFY_TOKEN_URL = "https://accounts.spotify.com/api/";

    @POST("token")
    Call <SpotifyToken> getSpotifyToken(@Query("grant_type") String grant_type,
                                        @Header("Content-Type") String contentType,
                                        @Header("Authorization") String authHeader);

    @GET("browse/new-releases")
    Call <SpotifyNewRelease> getNewReleases(@Query("country") String country,
                                            @Query("limit") String limit,
                                            @Header("Authorization") String authHeader);

    @GET("search")
    Call <SpotifySearchByTrack> browseTrack(@Query("q") String q,
                                            @Query("type") String type,
                                            @Header("Content-Type") String contentType,
                                            @Header("Authorization") String authHeader);

    @GET("search")
    Call <SpotifySearchByArtist> browseArtist(@Query("q") String q,
                                              @Query("type") String type,
                                              @Header("Authorization") String authHeader);

    @GET()
    Call<SpotifyAudioFeatures> getAudioFeatures(@Url() String url, @Header("Authorization") String authHeader);

    @GET()
    Call<SpotifyTracksInAlbum> getTracklist(@Url() String url,
                                            @Header("Content-Type") String contentType,
                                            @Header("Authorization") String authHeader);

}
