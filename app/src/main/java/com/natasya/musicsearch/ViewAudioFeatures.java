package com.natasya.musicsearch;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.natasya.musicsearch.spotifymodel.SpotifyAudioFeatures;

public class ViewAudioFeatures extends AppCompatActivity {

    SpotifyAudioFeatures features;
    String track, artist, trackID;
    TextView danceability, energy, loudness, speechiness, acousticness, instrumentalness, liveness, valence, tempo, timeSignature;
    TextView artistName, trackName, spotifyLinkText;
    LinearLayout closeBtn, spotifyLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_analytics);

        Intent intent = getIntent();
        features = (SpotifyAudioFeatures) intent.getSerializableExtra("audioFeatures");
        track= (String) intent.getSerializableExtra("trackName");
        artist= (String) intent.getSerializableExtra("artistName");
        trackID = (String) intent.getSerializableExtra("trackID");
        danceability = findViewById(R.id.danceability);
        energy = findViewById(R.id.energy);
        loudness = findViewById(R.id.loudness);
        speechiness = findViewById(R.id.speechiness);
        acousticness = findViewById(R.id.acousticness);
        instrumentalness = findViewById(R.id.instrumentalness);
        liveness = findViewById(R.id.liveness);
        valence = findViewById(R.id.valence);
        tempo = findViewById(R.id.tempo);
        timeSignature = findViewById(R.id.timeSignature);
        trackName = findViewById(R.id.trackName);
        artistName = findViewById(R.id.artistName);
        closeBtn = findViewById(R.id.closeBtn);

        spotifyLink = findViewById(R.id.spotifyLink);
        spotifyLinkText = findViewById(R.id.spotifyLinkText);

        danceability.setText(features.getDanceability());
        energy.setText(features.getEnergy());
        loudness.setText(features.getLoudness());
        speechiness.setText(features.getSpeechiness());
        acousticness.setText(features.getAcousticness());
        instrumentalness.setText(features.getInstrumentalness());
        liveness.setText(features.getLiveness());
        valence.setText(features.getValence());
        tempo.setText(features.getTempo());
        timeSignature.setText(features.getTime_signature());
        trackName.setText(track);
        artistName.setText(artist);


        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        spotifyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getPackageManager();
                boolean isSpotifyInstalled;
                try {
                    pm.getPackageInfo("com.spotify.music", 0);
                    isSpotifyInstalled = true;
                } catch (PackageManager.NameNotFoundException e) {
                    isSpotifyInstalled = false;
                }

                if (isSpotifyInstalled == true){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("spotify:track:" + trackID));
                    intent.putExtra(Intent.EXTRA_REFERRER,
                            Uri.parse("android-app://" + getApplicationContext().getPackageName()));
                    startActivity(intent);
                    //Toast.makeText(ViewAudioFeatures.this, "Installed!", Toast.LENGTH_SHORT).show();
                }else{

                    final String appPackageName = "com.spotify.music";
                    final String referrer = "adjust_campaign=PACKAGE_NAME&adjust_tracker=ndjczk&utm_source=adjust_preinstall";

                    try {
                        Uri uri = Uri.parse("market://details")
                                .buildUpon()
                                .appendQueryParameter("id", appPackageName)
                                .appendQueryParameter("referrer", referrer)
                                .build();
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    } catch (android.content.ActivityNotFoundException ignored) {
                        Uri uri = Uri.parse("https://play.google.com/store/apps/details")
                                .buildUpon()
                                .appendQueryParameter("id", appPackageName)
                                .appendQueryParameter("referrer", referrer)
                                .build();
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    }
                    //Toast.makeText(ViewAudioFeatures.this, "Not Installed!", Toast.LENGTH_SHORT).show();
                }

            }
        });


        PackageManager pm = getPackageManager();
        boolean isSpotifyInstalled;
        try {
            pm.getPackageInfo("com.spotify.music", 0);
            isSpotifyInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            isSpotifyInstalled = false;
        }

        if(isSpotifyInstalled == true){
            spotifyLinkText.setText("OPEN SPOTIFY");
        }else{
            spotifyLinkText.setText("GET SPOTIFY FREE");
        }


    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
