package com.natasya.musicsearch;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.natasya.musicsearch.adapter.ListAlbumsAdapter;
import com.natasya.musicsearch.adapter.ListTracksAdapter;
import com.natasya.musicsearch.api.Api;
import com.natasya.musicsearch.spotifymodel.SpotifyItems;
import com.natasya.musicsearch.spotifymodel.SpotifyNewRelease;
import com.natasya.musicsearch.spotifymodel.SpotifySearchByTrack;
import com.natasya.musicsearch.spotifymodel.SpotifyToken;
import com.natasya.musicsearch.spotifymodel.SpotifyTrackItems;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BrowseTrack extends AppCompatActivity {

    EditText searchBar;
    BottomNavigationView bottomNav;
    LoadingDialog loadingDialog;
    LinearLayout refreshBtn, spotifyLink, spotifyLinkLayout, searchBtn;
    ImageView musicImg;
    TextView message, spotifyLinkText;
    ListView listTracks;
    ListTracksAdapter listTracksAdapter;
    DBHandler db;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_track);

        searchBar = findViewById(R.id.searchBar);
        bottomNav = findViewById(R.id.bottomNav);
        loadingDialog = new LoadingDialog(BrowseTrack.this);
        refreshBtn = findViewById(R.id.refreshBtn);
        musicImg = findViewById(R.id.musicImg);
        message = findViewById(R.id.message);
        listTracks = findViewById(R.id.listTracks);

        spotifyLink = findViewById(R.id.spotifyLink);
        spotifyLinkText = findViewById(R.id.spotifyLinkText);
        spotifyLinkLayout = findViewById(R.id.spotifyLinkLayout);
        searchBtn = findViewById(R.id.searchBtn);
        db = new DBHandler(BrowseTrack.this);

        Cursor authData = db.getTokenData(db.TABLE_NAME);
        if(authData.getCount()!=0){
            if(authData.moveToFirst()){

                token = authData.getString(0);
            }
        }

        searchBar.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        searchBar.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                {
                    if(searchBar.getText().length()!=0){
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        getTracks(searchBar.getText()+"", token, "init");
                        return true;
                    }

                }
                return false;
            }


        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchBar.getText().length()!=0){
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    getTracks(searchBar.getText()+"", token, "init");
                }

            }
        });

        bottomNav.setSelectedItemId(R.id.trackNav);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.newAlbumNav:
                        finish();
                        Intent intent = new Intent(BrowseTrack.this, NewRelease.class);
                        startActivity(intent);
                        break;

                    case R.id.trackNav:

                        break;

                    case R.id.artistNav:
                        finish();
                        Intent intent2 = new Intent(BrowseTrack.this, BrowseArtist.class);
                        startActivity(intent2);
                       break;

                }

                return false;
            }
        });



        if(listTracks.isShown()){
            spotifyLinkLayout.setVisibility(View.VISIBLE);
        }else{
            spotifyLinkLayout.setVisibility(View.GONE);
        }

    }

    public void getTracks(final String input, String token, String menu){

        if(menu.equals("init")){
            loadingDialog.onLoading();
        }

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_API_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);

        Call<SpotifySearchByTrack> call = api.browseTrack(input, "track", "application/json", "Bearer " + token);

        call.enqueue(new Callback<SpotifySearchByTrack>() {
            @Override
            public void onResponse(Call<SpotifySearchByTrack> call, Response<SpotifySearchByTrack> response) {

                SpotifySearchByTrack track = response.body();
                if(response.code()!= 200){

                        loadingDialog.stopLoading();
                        musicImg.setVisibility(View.VISIBLE);
                        message.setVisibility(View.VISIBLE);
                        message.setText("Sorry, please refresh to try again!");
                        listTracks.setVisibility(View.GONE);
                        refreshBtn.setVisibility(View.VISIBLE);
                        refreshBtn.setEnabled(true);
                        setSpotifyLinkLayout();
                        refreshBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadingDialog.onLoading();
                                getToken(input);
                            }
                        });


                }else{
                    loadingDialog.stopLoading();


                    if(track.getTracks().getItems().size() != 0){
                        final List<SpotifyTrackItems> results = track.getTracks().getItems();
                        listTracksAdapter = new ListTracksAdapter(BrowseTrack.this, R.layout.row_track, results);
                        listTracks.setAdapter(listTracksAdapter);
                        listTracks.setVisibility(View.VISIBLE);
                        musicImg.setVisibility(View.GONE);
                        message.setVisibility(View.GONE);
                        refreshBtn.setVisibility(View.GONE);

                        spotifyLink.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PackageManager pm = getPackageManager();
                                boolean isSpotifyInstalled;
                                try {
                                    pm.getPackageInfo("com.spotify.music", 0);
                                    isSpotifyInstalled = true;
                                } catch (PackageManager.NameNotFoundException e) {
                                    isSpotifyInstalled = false;
                                }

                                if (isSpotifyInstalled == true){
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse("spotify:track:" + results.get(0).getId()));
                                    intent.putExtra(Intent.EXTRA_REFERRER,
                                            Uri.parse("android-app://" + BrowseTrack.this.getPackageName()));
                                    startActivity(intent);
                                   //Toast.makeText(BrowseTrack.this, "Installed!", Toast.LENGTH_SHORT).show();
                                }else{

                                    final String appPackageName = "com.spotify.music";
                                    final String referrer = "adjust_campaign=PACKAGE_NAME&adjust_tracker=ndjczk&utm_source=adjust_preinstall";

                                    try {
                                        Uri uri = Uri.parse("market://details")
                                                .buildUpon()
                                                .appendQueryParameter("id", appPackageName)
                                                .appendQueryParameter("referrer", referrer)
                                                .build();
                                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    } catch (android.content.ActivityNotFoundException ignored) {
                                        Uri uri = Uri.parse("https://play.google.com/store/apps/details")
                                                .buildUpon()
                                                .appendQueryParameter("id", appPackageName)
                                                .appendQueryParameter("referrer", referrer)
                                                .build();
                                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                                    }
                                    //Toast.makeText(BrowseTrack.this, "Not Installed!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });


                        PackageManager pm = getPackageManager();
                        boolean isSpotifyInstalled;
                        try {
                            pm.getPackageInfo("com.spotify.music", 0);
                            isSpotifyInstalled = true;
                        } catch (PackageManager.NameNotFoundException e) {
                            isSpotifyInstalled = false;
                        }

                        if(isSpotifyInstalled == true){
                            spotifyLinkText.setText("OPEN SPOTIFY");
                        }else{
                            spotifyLinkText.setText("GET SPOTIFY FREE");
                        }

                        setSpotifyLinkLayout();

                    }else{
                        listTracks.setVisibility(View.GONE);
                        musicImg.setVisibility(View.VISIBLE);
                        refreshBtn.setVisibility(View.GONE);
                        message.setVisibility(View.VISIBLE);
                        message.setText("No data found.");

                        setSpotifyLinkLayout();
                    }

                }

            }

            @Override
            public void onFailure(Call<SpotifySearchByTrack> call, Throwable t) {
                loadingDialog.stopLoading();
                musicImg.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                message.setText("Sorry, please refresh to try again!");
                listTracks.setVisibility(View.GONE);
                refreshBtn.setVisibility(View.VISIBLE);
                refreshBtn.setEnabled(true);
                setSpotifyLinkLayout();
                refreshBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingDialog.onLoading();
                        getToken(input);
                    }
                });
                t.getMessage();

            }
        });


    }

    public void setSpotifyLinkLayout(){
        if(listTracks.isShown()){
            spotifyLinkLayout.setVisibility(View.VISIBLE);
        }else{
            spotifyLinkLayout.setVisibility(View.GONE);
        }
    }
    public void getToken(final String input){

        //loadingDialog.onLoading();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_TOKEN_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyToken> call = api.getSpotifyToken("client_credentials", "application/x-www-form-urlencoded", "Basic NTdmMmI1ZThhMWEyNGNiYjkxYjY2NTFkM2Q0ZDhkYmY6M2IyNTE0ZWMzNDMwNDBiMDgxNmQ4NzA3M2I1ZTQ0YjU=");
        call.enqueue(new Callback<SpotifyToken>() {
            @Override
            public void onResponse(Call<SpotifyToken> call, Response<SpotifyToken> response) {

                SpotifyToken token = response.body();
                if (response.code() != 200) {
                    loadingDialog.stopLoading();
                    musicImg.setVisibility(View.VISIBLE);
                    message.setVisibility(View.VISIBLE);
                    message.setText("Sorry, please refresh to try again!");
                    listTracks.setVisibility(View.GONE);
                    refreshBtn.setVisibility(View.VISIBLE);
                    refreshBtn.setEnabled(true);
                    refreshBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadingDialog.onLoading();
                            getToken(input);
                        }
                    });
                    setSpotifyLinkLayout();
                } else {
                    Cursor cursor = db.getTokenData(db.TABLE_NAME);
                    if(cursor.getCount()==0){
                        db.insertAuthData(token.getAccess_token());
                    }else{
                        db.deleteData(db.TABLE_NAME);
                        db.insertAuthData(token.getAccess_token());
                    }
                    musicImg.setVisibility(View.VISIBLE);
                    message.setVisibility(View.VISIBLE);
                    message.setText("Sorry, please refresh to try again!");
                    refreshBtn.setVisibility(View.VISIBLE);
                    listTracks.setVisibility(View.GONE);
                    getTracks(input, token.getAccess_token(), "");
                    setSpotifyLinkLayout();
                }

            }

            @Override
            public void onFailure(Call<SpotifyToken> call, Throwable t) {
                loadingDialog.stopLoading();
                musicImg.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                message.setText("Sorry, please refresh to try again!");
                listTracks.setVisibility(View.GONE);
                refreshBtn.setVisibility(View.VISIBLE);
                refreshBtn.setEnabled(true);
                setSpotifyLinkLayout();
                refreshBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadingDialog.onLoading();
                        getToken(input);
                    }
                });

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(BrowseTrack.this, NewRelease.class);
        startActivity(intent);
    }


}
