package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifyExtURL implements Serializable {

    private String spotify;

    public SpotifyExtURL(String spotify) {
        this.spotify = spotify;
    }

    public String getSpotify() {
        return spotify;
    }

    public void setSpotify(String spotify) {
        this.spotify = spotify;
    }
}
