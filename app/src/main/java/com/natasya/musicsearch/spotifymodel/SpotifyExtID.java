package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifyExtID implements Serializable {

    private String isrc;

    public SpotifyExtID(String isrc) {
        this.isrc = isrc;
    }

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }
}
