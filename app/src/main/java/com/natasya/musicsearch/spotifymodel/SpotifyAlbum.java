package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;
import java.util.List;

public class SpotifyAlbum implements Serializable {

    private String href;
    private List<SpotifyItems> items;
    private String limit;
    private String next;
    private String offset;
    private String previous;
    private String total;

    public SpotifyAlbum(String href, List<SpotifyItems> items, String limit, String next, String offset, String previous, String total) {
        this.href = href;
        this.items = items;
        this.limit = limit;
        this.next = next;
        this.offset = offset;
        this.previous = previous;
        this.total = total;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<SpotifyItems> getItems() {
        return items;
    }

    public void setItems(List<SpotifyItems> items) {
        this.items = items;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
