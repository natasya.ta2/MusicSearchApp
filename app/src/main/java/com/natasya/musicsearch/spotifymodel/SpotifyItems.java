package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;
import java.util.List;

public class SpotifyItems implements Serializable {

    private String album_type;
    private List<SpotifyArtist> artists;
    private List<String> available_markets;
    private SpotifyExtURL external_urls;
    private String href;
    private String id;
    private List <SpotifyImages> images;
    private String name;
    private String release_date;
    private String release_date_precision;
    private String total_tracks;
    private String type;
    private String uri;

    public SpotifyItems(String album_type, List<SpotifyArtist> artists, List<String> available_markets, SpotifyExtURL external_urls, String href, String id, List<SpotifyImages> images, String name, String release_date, String release_date_precision, String total_tracks, String type, String uri) {
        this.album_type = album_type;
        this.artists = artists;
        this.available_markets = available_markets;
        this.external_urls = external_urls;
        this.href = href;
        this.id = id;
        this.images = images;
        this.name = name;
        this.release_date = release_date;
        this.release_date_precision = release_date_precision;
        this.total_tracks = total_tracks;
        this.type = type;
        this.uri = uri;
    }

    public String getAlbum_type() {
        return album_type;
    }

    public void setAlbum_type(String album_type) {
        this.album_type = album_type;
    }

    public List<SpotifyArtist> getArtists() {
        return artists;
    }

    public void setArtists(List<SpotifyArtist> artists) {
        this.artists = artists;
    }

    public List<String> getAvailable_markets() {
        return available_markets;
    }

    public void setAvailable_markets(List<String> available_markets) {
        this.available_markets = available_markets;
    }

    public SpotifyExtURL getExternal_urls() {
        return external_urls;
    }

    public void setExternal_urls(SpotifyExtURL external_urls) {
        this.external_urls = external_urls;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<SpotifyImages> getImages() {
        return images;
    }

    public void setImages(List<SpotifyImages> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRelease_date_precision() {
        return release_date_precision;
    }

    public void setRelease_date_precision(String release_date_precision) {
        this.release_date_precision = release_date_precision;
    }

    public String getTotal_tracks() {
        return total_tracks;
    }

    public void setTotal_tracks(String total_tracks) {
        this.total_tracks = total_tracks;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
