package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifyNewRelease implements Serializable {

    private SpotifyAlbum albums;

    public SpotifyNewRelease(SpotifyAlbum albums) {
        this.albums = albums;
    }

    public SpotifyAlbum getAlbums() {
        return albums;
    }

    public void setAlbums(SpotifyAlbum albums) {
        this.albums = albums;
    }
}
