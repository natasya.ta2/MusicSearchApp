package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifyFollowers implements Serializable {

    private String href;
    private String total;

    public SpotifyFollowers(String href, String total) {
        this.href = href;
        this.total = total;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
