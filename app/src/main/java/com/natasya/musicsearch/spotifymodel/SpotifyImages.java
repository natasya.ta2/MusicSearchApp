package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifyImages implements Serializable {

    private String height;
    private String url;
    private String width;

    public SpotifyImages(String height, String url, String width) {
        this.height = height;
        this.url = url;
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
