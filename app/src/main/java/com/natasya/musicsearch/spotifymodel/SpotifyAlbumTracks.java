package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;
import java.util.List;

public class SpotifyAlbumTracks implements Serializable {

    private List<SpotifyArtist> artists;
    private List<String> available_markets;
    private String disc_number;
    private int duration_ms;
    private Boolean explicit;
    private SpotifyExtURL external_urls;
    private String href;
    private String id;
    private Boolean is_local;
    private String name;
    private String preview_url;
    private String track_number;
    private String type;
    private String uri;

    public SpotifyAlbumTracks(List<SpotifyArtist> artists, List<String> available_markets, String disc_number, int duration_ms, Boolean explicit, SpotifyExtURL external_urls, String href, String id, Boolean is_local, String name, String preview_url, String track_number, String type, String uri) {
        this.artists = artists;
        this.available_markets = available_markets;
        this.disc_number = disc_number;
        this.duration_ms = duration_ms;
        this.explicit = explicit;
        this.external_urls = external_urls;
        this.href = href;
        this.id = id;
        this.is_local = is_local;
        this.name = name;
        this.preview_url = preview_url;
        this.track_number = track_number;
        this.type = type;
        this.uri = uri;
    }

    public List<SpotifyArtist> getArtists() {
        return artists;
    }

    public void setArtists(List<SpotifyArtist> artists) {
        this.artists = artists;
    }

    public List<String> getAvailable_markets() {
        return available_markets;
    }

    public void setAvailable_markets(List<String> available_markets) {
        this.available_markets = available_markets;
    }

    public String getDisc_number() {
        return disc_number;
    }

    public void setDisc_number(String disc_number) {
        this.disc_number = disc_number;
    }

    public int getDuration_ms() {
        return duration_ms;
    }

    public void setDuration_ms(int duration_ms) {
        this.duration_ms = duration_ms;
    }

    public Boolean getExplicit() {
        return explicit;
    }

    public void setExplicit(Boolean explicit) {
        this.explicit = explicit;
    }

    public SpotifyExtURL getExternal_urls() {
        return external_urls;
    }

    public void setExternal_urls(SpotifyExtURL external_urls) {
        this.external_urls = external_urls;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIs_local() {
        return is_local;
    }

    public void setIs_local(Boolean is_local) {
        this.is_local = is_local;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public void setPreview_url(String preview_url) {
        this.preview_url = preview_url;
    }

    public String getTrack_number() {
        return track_number;
    }

    public void setTrack_number(String track_number) {
        this.track_number = track_number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
