package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifySearchByTrack implements Serializable {

    private SpotifyTracks tracks;

    public SpotifySearchByTrack(SpotifyTracks tracks) {
        this.tracks = tracks;
    }

    public SpotifyTracks getTracks() {
        return tracks;
    }

    public void setTracks(SpotifyTracks tracks) {
        this.tracks = tracks;
    }
}
