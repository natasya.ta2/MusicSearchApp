package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;

public class SpotifySearchByArtist implements Serializable {

    private SpotifyArtists artists;

    public SpotifySearchByArtist(SpotifyArtists artists) {
        this.artists = artists;
    }

    public SpotifyArtists getArtists() {
        return artists;
    }

    public void setArtists(SpotifyArtists artists) {
        this.artists = artists;
    }
}
