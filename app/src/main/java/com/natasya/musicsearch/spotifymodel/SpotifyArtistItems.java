package com.natasya.musicsearch.spotifymodel;

import java.io.Serializable;
import java.util.List;

public class SpotifyArtistItems implements Serializable {

    private SpotifyExtURL external_urls;
    private SpotifyFollowers followers;
    private List<String> genres;
    private String href;
    private String id;
    private List<SpotifyImages> images;
    private String name;
    private String popularity;
    private String type;
    private String uri;

    public SpotifyArtistItems(SpotifyExtURL external_urls, SpotifyFollowers followers, List<String> genres, String href, String id, List<SpotifyImages> images, String name, String popularity, String type, String uri) {
        this.external_urls = external_urls;
        this.followers = followers;
        this.genres = genres;
        this.href = href;
        this.id = id;
        this.images = images;
        this.name = name;
        this.popularity = popularity;
        this.type = type;
        this.uri = uri;
    }

    public SpotifyExtURL getExternal_urls() {
        return external_urls;
    }

    public void setExternal_urls(SpotifyExtURL external_urls) {
        this.external_urls = external_urls;
    }

    public SpotifyFollowers getFollowers() {
        return followers;
    }

    public void setFollowers(SpotifyFollowers followers) {
        this.followers = followers;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<SpotifyImages> getImages() {
        return images;
    }

    public void setImages(List<SpotifyImages> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
