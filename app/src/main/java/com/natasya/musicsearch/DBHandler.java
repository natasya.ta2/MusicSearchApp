package com.natasya.musicsearch;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHandler extends SQLiteOpenHelper {

    public SQLiteDatabase vSql;

    //TABLE USER
    public static final String DATABASE_NAME = "auth.db";
    public static final String TABLE_NAME = "Authentication";
    public static final String column1 = "Token";


    public DBHandler(Context context){
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db =this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "+TABLE_NAME+" (Token TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public Cursor getTokenData(String table_name){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM "+table_name,null);
        return res;
    }


    public boolean insertAuthData(String token){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(column1, token);

        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result==-1){
            return false;
        }else {
            return true;
        }
    }


    public Cursor getCursorData(String vQuery){
        return vSql.rawQuery(vQuery,null);
    }

    public void deleteData(String table_name){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table_name, null, null);
    }



}
