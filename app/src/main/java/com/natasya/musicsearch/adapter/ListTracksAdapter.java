package com.natasya.musicsearch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.natasya.musicsearch.DBHandler;
import com.natasya.musicsearch.R;
import com.natasya.musicsearch.ViewAudioFeatures;
import com.natasya.musicsearch.api.Api;
import com.natasya.musicsearch.spotifymodel.SpotifyArtist;
import com.natasya.musicsearch.spotifymodel.SpotifyAudioFeatures;
import com.natasya.musicsearch.spotifymodel.SpotifyToken;
import com.natasya.musicsearch.spotifymodel.SpotifyTrackItems;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListTracksAdapter extends BaseAdapter{

    List<SpotifyTrackItems> items, tempArray;
    Context context;
    int resource;
    DBHandler db;
    String token;
    LoadingDialog loadingDialog;

        public ListTracksAdapter(@NonNull Context context, int resource, @NonNull List<SpotifyTrackItems> items) {

        this.items = items;
        this.tempArray = items;
        this.context = context;
        this.resource = resource;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<SpotifyTrackItems> getListItems(){
        return items;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, final @NonNull ViewGroup parent) {

        loadingDialog = new LoadingDialog((Activity)parent.getContext());
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_track, null, true);
        }

        TextView trackName = convertView.findViewById(R.id.trackName);
        trackName.setText(items.get(position).getName());

        TextView artistName = convertView.findViewById(R.id.artistName);


        String artists = "";
        for(int i=0; i<items.get(position).getArtists().size(); i++){
            if(items.get(position).getArtists().get(i).getName() != null){
                if(i == items.get(position).getArtists().size()-1){
                    artists += items.get(position).getArtists().get(i).getName();
                }else{
                    artists += items.get(position).getArtists().get(i).getName() +", ";
                }
            }
        }
        artistName.setText(artists);

        TextView albumName = convertView.findViewById(R.id.albumName);
        albumName.setText(items.get(position).getAlbum().getName());

        TextView trackNumber = convertView.findViewById(R.id.trackNo);
        trackNumber.setText(items.get(position).getTrack_number());

        TextView duration = convertView.findViewById(R.id.duration);


        double seconds = (((double)items.get(position).getDuration_ms())/1000) % 60;
        long secondVal = (((long)items.get(position).getDuration_ms())/1000) % 60;
        String secondStr = String.valueOf(seconds);
        secondStr = secondStr.substring(secondStr.indexOf(".")).substring(1,2);

        if(Integer.parseInt(secondStr)>=5){
            secondVal = secondVal + 1;
        }
        long minutes = (((long)items.get(position).getDuration_ms())/1000)/60;
        if (secondVal<10){
            duration.setText(minutes+ ":0" + secondVal);
        }else{

            duration.setText(minutes+ ":" + secondVal);
        }

        TextView explicitStatus = convertView.findViewById(R.id.explicitStatus);
        if(items.get(position).getExplicit() == true){
            explicitStatus.setText("EXPLICIT");
        }else{
            explicitStatus.setText("");
        }

        ImageView photo = convertView.findViewById(R.id.image);
        if(items.get(position).getAlbum().getImages().size() >= 1){
            photo.setVisibility(View.VISIBLE);
            String URL = items.get(position).getAlbum().getImages().get(0).getUrl();
            Picasso.get().load(URL).into(photo);
        }else{
            photo.setVisibility(View.INVISIBLE);
        }


        db = new DBHandler(parent.getContext());
        Cursor authData = db.getTokenData(db.TABLE_NAME);
        if(authData.getCount()!=0){
            if(authData.moveToFirst()){

                token = authData.getString(0);
            }
        }

        LinearLayout analyticsBtn = convertView.findViewById(R.id.analyticsBtn);
        analyticsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFeatures(token, (Activity)parent.getContext(), items.get(position).getName(), items.get(position).getArtists(), items.get(position).getId(), "init");
            }
        });

        return convertView;
    }


    public void getFeatures(String token, final Activity activity, final String track, final List<SpotifyArtist> artist, final String trackID, String menu){

        //final LoadingDialog loadingDialog = new LoadingDialog(activity);

        if(menu.equals("init")){
            loadingDialog.onLoading();
        }

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_API_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyAudioFeatures> call = api.getAudioFeatures("https://api.spotify.com/v1/audio-features/" + trackID, "Bearer " + token);

        call.enqueue(new Callback<SpotifyAudioFeatures>() {
            @Override
            public void onResponse(Call<SpotifyAudioFeatures> call, Response<SpotifyAudioFeatures> response) {

                SpotifyAudioFeatures features = response.body();
                if(response.code()!= 200){
                    if(response.code() == 401){
                        //loadingDialog.stopLoading();
                        getToken(activity, track, artist, trackID);
                    }else{
                        loadingDialog.stopLoading();
                        Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    loadingDialog.stopLoading();
                    Intent intent = new Intent(context, ViewAudioFeatures.class);
                    intent.putExtra("trackName", track);
                    String artists = "";
                    for(int i=0; i<artist.size(); i++){
                        if(artist.get(i).getName() != null){
                            if(i == artist.size()-1){
                                artists += artist.get(i).getName();
                            }else{
                                artists += artist.get(i).getName() +", ";
                            }
                        }
                    }
                    intent.putExtra("artistName", artists);
                    intent.putExtra("audioFeatures", features);
                    intent.putExtra("trackID", trackID);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    //Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpotifyAudioFeatures> call, Throwable t) {
                loadingDialog.stopLoading();
                Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void getToken(final Activity activity, final String track, final List<SpotifyArtist> artist, final String trackID){

        //final LoadingDialog loadingDialog = new LoadingDialog(activity);
        //loadingDialog.onLoading();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_TOKEN_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyToken> call = api.getSpotifyToken("client_credentials", "application/x-www-form-urlencoded", "Basic NTdmMmI1ZThhMWEyNGNiYjkxYjY2NTFkM2Q0ZDhkYmY6M2IyNTE0ZWMzNDMwNDBiMDgxNmQ4NzA3M2I1ZTQ0YjU=");
        call.enqueue(new Callback<SpotifyToken>() {
            @Override
            public void onResponse(Call<SpotifyToken> call, Response<SpotifyToken> response) {

                SpotifyToken token = response.body();
                if (response.code() != 200) {
                    loadingDialog.stopLoading();
                    Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
                } else {

                    //loadingDialog.stopLoading();
                    db = new DBHandler(context);
                    Cursor cursor = db.getTokenData(db.TABLE_NAME);
                    if(cursor.getCount()==0){
                        db.insertAuthData(token.getAccess_token());
                    }else{
                        db.deleteData(db.TABLE_NAME);
                        db.insertAuthData(token.getAccess_token());
                    }
                    getFeatures(token.getAccess_token(), activity, track, artist, trackID, "");
                }

            }

            @Override
            public void onFailure(Call<SpotifyToken> call, Throwable t) {

                loadingDialog.stopLoading();
                Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}

