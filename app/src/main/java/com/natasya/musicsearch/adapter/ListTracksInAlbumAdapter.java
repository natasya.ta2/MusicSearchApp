package com.natasya.musicsearch.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.natasya.musicsearch.R;
import com.natasya.musicsearch.spotifymodel.SpotifyAlbumTracks;

import java.util.ArrayList;

public class ListTracksInAlbumAdapter extends BaseAdapter{

    ArrayList<SpotifyAlbumTracks> items, tempArray;
    Context context;
    int resource;


        public ListTracksInAlbumAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SpotifyAlbumTracks> items) {

        this.items = items;
        this.tempArray = items;
        this.context = context;
        this.resource = resource;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<SpotifyAlbumTracks> getListItems(){
        return items;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, final @NonNull ViewGroup parent) {

        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_track_list, null, true);
        }

        TextView trackName = convertView.findViewById(R.id.trackName);
        trackName.setText(items.get(position).getName());


        TextView duration = convertView.findViewById(R.id.duration);


        double seconds = (((double)items.get(position).getDuration_ms())/1000) % 60;
        long secondVal = (((long)items.get(position).getDuration_ms())/1000) % 60;
        String secondStr = String.valueOf(seconds);
        secondStr = secondStr.substring(secondStr.indexOf(".")).substring(1,2);

        if(Integer.parseInt(secondStr)>=5){
            secondVal = secondVal + 1;
        }
        long minutes = (((long)items.get(position).getDuration_ms())/1000)/60;
        if (secondVal<10){
            duration.setText(minutes+ ":0" + secondVal);
        }else{

            duration.setText(minutes+ ":" + secondVal);
        }

        TextView explicitStatus = convertView.findViewById(R.id.explicitStatus);
        if(items.get(position).getExplicit() == true){
            explicitStatus.setText("EXPLICIT");
        }else{
            explicitStatus.setText("");
        }

        TextView trackNumber = convertView.findViewById(R.id.number);
        trackNumber.setText(items.get(position).getTrack_number());

        return convertView;
    }



}

