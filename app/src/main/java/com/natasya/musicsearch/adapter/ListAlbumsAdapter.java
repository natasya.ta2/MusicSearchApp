package com.natasya.musicsearch.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.natasya.musicsearch.DBHandler;
import com.natasya.musicsearch.R;
import com.natasya.musicsearch.ViewAudioFeatures;
import com.natasya.musicsearch.ViewTracks;
import com.natasya.musicsearch.api.Api;
import com.natasya.musicsearch.spotifymodel.SpotifyArtist;
import com.natasya.musicsearch.spotifymodel.SpotifyAudioFeatures;
import com.natasya.musicsearch.spotifymodel.SpotifyItems;
import com.natasya.musicsearch.spotifymodel.SpotifyToken;
import com.natasya.musicsearch.spotifymodel.SpotifyTrackItems;
import com.natasya.musicsearch.spotifymodel.SpotifyTracksInAlbum;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListAlbumsAdapter extends BaseAdapter{

    List<SpotifyItems> items, tempArray;
    Context context;
    int resource;
    DBHandler db;
    String token;
    LoadingDialog loadingDialog;

        public ListAlbumsAdapter(@NonNull Context context, int resource, @NonNull List<SpotifyItems> items) {

        this.items = items;
        this.tempArray = items;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<SpotifyItems> getListItems(){
        return items;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, final @NonNull ViewGroup parent) {

        loadingDialog = new LoadingDialog((Activity)parent.getContext());
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_album, null, true);
        }

        TextView albumName = convertView.findViewById(R.id.albumName);
        albumName.setText(items.get(position).getName());

        TextView artistName = convertView.findViewById(R.id.artistName);

        String artists = "";
        for(int i=0; i<items.get(position).getArtists().size(); i++){
            if(items.get(position).getArtists().get(i).getName() != null){
                if(i == items.get(position).getArtists().size()-1){
                    artists += items.get(position).getArtists().get(i).getName();
                }else{
                    artists += items.get(position).getArtists().get(i).getName() +", ";
                }
            }
        }
        artistName.setText(artists);

        TextView releaseDate = convertView.findViewById(R.id.releaseDate);
        releaseDate.setText(items.get(position).getRelease_date());

        TextView totalTracks = convertView.findViewById(R.id.trackCount);
        totalTracks.setText(items.get(position).getTotal_tracks());

        ImageView photo = convertView.findViewById(R.id.image);
        if(items.get(position).getImages().size() >= 1){
            photo.setVisibility(View.VISIBLE);
            String URL = items.get(position).getImages().get(0).getUrl();
            Picasso.get().load(URL).into(photo);
        }else{
            photo.setVisibility(View.INVISIBLE);
        }

        db = new DBHandler(parent.getContext());
        Cursor authData = db.getTokenData(db.TABLE_NAME);
        if(authData.getCount()!=0){
            if(authData.moveToFirst()){

                token = authData.getString(0);
            }
        }

        LinearLayout tracklistBtn = convertView.findViewById(R.id.tracklistBtn);
        tracklistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTracklist(token, (Activity)parent.getContext(), items.get(position).getName(), items.get(position).getArtists(), items.get(position).getId(), "init");
            }
        });


        return convertView;
    }

    public void getTracklist( String token, final Activity activity, final String album, final List<SpotifyArtist> artist, final String albumID, String menu){

//        final LoadingDialog loadingDialog = new LoadingDialog(activity);

        if(menu.equals("init")){
            loadingDialog.onLoading();
        }

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_API_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyTracksInAlbum> call = api.getTracklist("https://api.spotify.com/v1/albums/" + albumID + "/tracks", "application/json", "Bearer " + token);

        call.enqueue(new Callback<SpotifyTracksInAlbum>() {
            @Override
            public void onResponse(Call<SpotifyTracksInAlbum> call, Response<SpotifyTracksInAlbum> response) {

                SpotifyTracksInAlbum tracklist = response.body();
                if(response.code()!= 200){
                    if(response.code() == 401){
                        //loadingDialog.stopLoading();
                        getToken(activity, album, artist, albumID);
                    }else {
                        loadingDialog.stopLoading();
                        Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    loadingDialog.stopLoading();
                    Intent intent = new Intent(context, ViewTracks.class);
                    intent.putExtra("albumName", album);
                    String artists = "";
                    for(int i=0; i<artist.size(); i++){
                        if(artist.get(i).getName() != null){
                            if(i == artist.size()-1){
                                artists += artist.get(i).getName();
                            }else{
                                artists += artist.get(i).getName() +", ";
                            }
                        }
                    }
                    intent.putExtra("artistName", artists);
                    intent.putExtra("albumID", albumID);
                    intent.putExtra("tracklist", tracklist.getItems());
                    //intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    //Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpotifyTracksInAlbum> call, Throwable t) {
                loadingDialog.stopLoading();
                Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void getToken(final Activity activity, final String album, final List<SpotifyArtist> artist, final String albumID){

        //final LoadingDialog loadingDialog = new LoadingDialog(activity);
        //loadingDialog.onLoading();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(30, TimeUnit.SECONDS);
        client.readTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.SPOTIFY_TOKEN_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build()).build();

        Api api = retrofit.create(Api.class);
        Call<SpotifyToken> call = api.getSpotifyToken("client_credentials", "application/x-www-form-urlencoded", "Basic NTdmMmI1ZThhMWEyNGNiYjkxYjY2NTFkM2Q0ZDhkYmY6M2IyNTE0ZWMzNDMwNDBiMDgxNmQ4NzA3M2I1ZTQ0YjU=");
        call.enqueue(new Callback<SpotifyToken>() {
            @Override
            public void onResponse(Call<SpotifyToken> call, Response<SpotifyToken> response) {

                SpotifyToken token = response.body();
                if (response.code() != 200) {
                    loadingDialog.stopLoading();
                    Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
                } else {

                    //loadingDialog.stopLoading();
                    db = new DBHandler(context);
                    Cursor cursor = db.getTokenData(db.TABLE_NAME);
                    if(cursor.getCount()==0){
                        db.insertAuthData(token.getAccess_token());
                    }else{
                        db.deleteData(db.TABLE_NAME);
                        db.insertAuthData(token.getAccess_token());
                    }
                    getTracklist(token.getAccess_token(), activity, album, artist, albumID, "");
                }

            }

            @Override
            public void onFailure(Call<SpotifyToken> call, Throwable t) {

                loadingDialog.stopLoading();
                Toast.makeText(context, "Sorry, please try again later!", Toast.LENGTH_SHORT).show();
            }
        });
    }



}
