package com.natasya.musicsearch.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.natasya.musicsearch.R;
import com.natasya.musicsearch.spotifymodel.SpotifyArtistItems;
import com.natasya.musicsearch.spotifymodel.SpotifyTrackItems;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class ListArtistsAdapter extends BaseAdapter{

    List<SpotifyArtistItems> items, tempArray;
    Context context;
    int resource;


        public ListArtistsAdapter(@NonNull Context context, int resource, @NonNull List<SpotifyArtistItems> items) {

        this.items = items;
        this.tempArray = items;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<SpotifyArtistItems> getListItems(){
        return items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_artist, null, true);
        }

        TextView genre = convertView.findViewById(R.id.genres);

        String genres = "";
        if(items.get(position).getGenres().size()!=0){
            for(int i=0; i<items.get(position).getGenres().size(); i++){
                if(items.get(position).getGenres().get(i) != null){
                    if(i == items.get(position).getGenres().size()-1){
                        genres += items.get(position).getGenres().get(i);
                    }else{
                        genres += items.get(position).getGenres().get(i) +", ";
                    }
                }
            }
            genre.setText(genres);
        }else{
            genre.setText("-");
        }




        TextView artistName = convertView.findViewById(R.id.artistName);
        artistName.setText(items.get(position).getName());

        TextView followers = convertView.findViewById(R.id.followers);
        followers.setText(NumberFormat.getInstance(Locale.US).format(Integer.parseInt(items.get(position).getFollowers().getTotal())));


        TextView popularityIndex = convertView.findViewById(R.id.popularityIndex);
        popularityIndex.setText(items.get(position).getPopularity());

        ImageView photo = convertView.findViewById(R.id.image);
        if(items.get(position).getImages().size() >= 1){
            photo.setVisibility(View.VISIBLE);
            String URL = items.get(position).getImages().get(0).getUrl();
            Picasso.get().load(URL).into(photo);
        }else{
            photo.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }


}
